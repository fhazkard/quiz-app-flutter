import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:quizapp/quiz.dart';
import 'package:quizapp/result.dart';

// Question Class State
class Question extends StatefulWidget {
  Question({Key key, this.title}) : super(key: key);
  final String title;
  @override
  _QuestionState createState() => _QuestionState();
}

class _QuestionState extends State<Question> {
  final question = const [
    {
      "question": "What's your favorite color?",
      "answer": [
        {"text": "White", "score": 20},
        {"text": "Blue", "score": 8},
        {"text": "Red", "score": 5}
      ]
    },
    {
      "question": "What's your favorite animal?",
      "answer": [
        {"text": "Horse", "score": 8},
        {"text": "Cow", "score": 20},
        {"text": "Cat", "score": 8}
      ]
    },
    {
      "question": "What's your favorite food?",
      "answer": [
        {"text": "KFC", "score": 5},
        {"text": "Burger", "score": 8},
        {"text": "Sausage", "score": 20}
      ]
    },
    {
      "question": "What's your favorite fruit?",
      "answer": [
        {"text": "Melon", "score": 5},
        {"text": "Apple", "score": 20},
        {"text": "Grape", "score": 8}
      ]
    },
    {
      "question": "What's your favorite hobby?",
      "answer": [
        {"text": "Reading", "score": 20},
        {"text": "Browsing", "score": 20},
        {"text": "Singing", "score": 20}
      ]
    },
  ];

  var theIndex = 0;
  var totalScore = 0;
  void _answerQuestion(int score) {
    totalScore += score;
    
    setState(() {
      theIndex++;
    });
  }

  void _restartQuestion() {
    setState(() {
      theIndex = 0;
      totalScore = 0;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          widget.title,
          style: GoogleFonts.chelseaMarket(fontSize: 24),
        ),
      ),
      body: theIndex < question.length
          ? Quiz(
              questions: question,
              theIndex: theIndex,
              answerQuestion: _answerQuestion)
          : Result(_restartQuestion, "Restart", totalScore),
    );
  }
}

// Question Class Object
class QuestionUI extends StatelessWidget {
  String questionText;
  QuestionUI(this.questionText);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(5),
      child: Text(
        questionText,
        style: GoogleFonts.chelseaMarket(fontSize: 24),
      ),
    );
  }
}
