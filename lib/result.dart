import 'package:flutter/material.dart';
import 'package:quizapp/question.dart';

class Result extends StatelessWidget {
  final Function restarQuestion;
  final String theAnswer;
  final int totalScore;
  Result(this.restarQuestion, this.theAnswer, this.totalScore);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
      children: <Widget>[
        QuestionUI("Your Score $totalScore"),
        RaisedButton(
          textColor: Colors.white,
          color: Colors.deepPurple,
          onPressed: restarQuestion,
          child: Text(theAnswer),
        ),
      ],
    ));
  }
}
