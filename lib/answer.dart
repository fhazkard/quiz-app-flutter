import 'package:flutter/material.dart';

class Answer extends StatelessWidget {
  final Function answerQuestion;
  final String theAnswer;
  Answer(this.answerQuestion, this.theAnswer);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      margin: EdgeInsets.fromLTRB(10, 2, 10, 2),
      child: RaisedButton(
        textColor: Colors.white,
        color: Colors.deepPurple,
        onPressed: answerQuestion,
        child: Text(theAnswer),
      ),
    );
  }
}
