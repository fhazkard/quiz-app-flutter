import 'package:flutter/material.dart';
import './question.dart';
import './answer.dart';

class Quiz extends StatelessWidget {
  final List<Map<String, Object>> questions;
  final Function answerQuestion;
  final int theIndex;

  Quiz(
      {@required this.questions,
      @required this.theIndex,
      @required this.answerQuestion});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          QuestionUI(questions[theIndex]['question']),
          ...(questions[theIndex]['answer'] as List<Map<String,Object>>).map((valueMap) {
            return Answer(()=> answerQuestion(valueMap['score']), valueMap['text']);
          }).toList()
        ],
      ),
    );
  }
}
